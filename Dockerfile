FROM golang:alpine as backend

RUN apk update && apk add --no-cache git ca-certificates tzdata && update-ca-certificates

COPY . /app
WORKDIR /app

RUN mkdir build && go build -o build/bot


FROM alpine

WORKDIR /app

COPY --from=backend /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=backend /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=backend /app/build/bot /app/

CMD ["/app/bot"]
