package bot

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"sort"
	"time"

	"github.com/robfig/cron/v3"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/oxymeal/clockify-report-bot/clockify"
	"golang.org/x/net/proxy"
)

type UnmarshalableDuration struct {
	time.Duration
}

func (d *UnmarshalableDuration) UnmarshalJSON(b []byte) error {
	var v interface{}
	if err := json.Unmarshal(b, &v); err != nil {
		return err
	}
	switch value := v.(type) {
	case string:
		var err error
		d.Duration, err = time.ParseDuration(value)
		if err != nil {
			return err
		}
		return nil
	default:
		return errors.New("invalid duration")
	}
}

type Config struct {
	ClockifyAPIKey  string
	WorkspaceIDs    []string
	BotToken        string
	TelegramProxy   string
	ChatID          int64
	HistoryInterval UnmarshalableDuration
	Timezone        string
	CronSchedule    string
	ReportTitle     string
}

type ClockifyStats struct {
	TopProjects []*ProjectStats
	TopUsers    []*UserStats
}

type ProjectStats struct {
	Project   *clockify.ProjectImpl
	TotalTime time.Duration
}

type UserStats struct {
	User      *clockify.User
	TotalTime time.Duration
}

type Bot struct {
	Config *Config
}

func (b *Bot) DownloadClockifyStats() (*ClockifyStats, error) {
	client := clockify.New(b.Config.ClockifyAPIKey)

	// Download data
	entries := []*clockify.TimeEntryImpl{}
	users := map[string]*clockify.User{}
	projects := map[string]*clockify.ProjectImpl{}
	for _, wid := range b.Config.WorkspaceIDs {
		// Collect projects
		workspaceProjects, err := client.GetWorkspaceProjects(wid)
		if err != nil {
			return nil, fmt.Errorf("GetWorkspaceProjects(%v): %w", wid, err)
		}

		for _, project := range workspaceProjects {
			projects[project.ID] = project
		}

		// Collect users
		workspaceUsers, err := client.GetWorkspaceUsers(wid)
		if err != nil {
			return nil, fmt.Errorf("GetWorkspaceUsers(%v): %w", wid, err)
		}

		for _, user := range workspaceUsers {
			users[user.ID] = user
		}

		// Collect time entries
		since := time.Now().Add(-b.Config.HistoryInterval.Duration)
		for _, user := range workspaceUsers {
			workspaceEntries, err := client.GetWorkspaceUserTimeEntries(wid, user.ID, since)
			if err != nil {
				return nil, fmt.Errorf("GetWorkspaceUserTimeEntries(%v, %v), user %#v: %w", wid, user.ID, user, err)
			}

			entries = append(entries, workspaceEntries...)
		}
	}

	// Calculate statistics
	userStatsMap := map[string]*UserStats{}
	projectStatsMap := map[string]*ProjectStats{}
	for _, entry := range entries {
		// Add entry to user statistics
		var userStats *UserStats
		if us, ok := userStatsMap[entry.UserID]; ok {
			userStats = us
		} else {
			userStats = &UserStats{User: users[entry.UserID], TotalTime: time.Duration(0)}
			userStatsMap[entry.UserID] = userStats
		}
		userStats.TotalTime += entry.TimeInterval.Duration

		// Add entry to project statistics
		var projectStats *ProjectStats
		if entry.ProjectID != nil {
			if ps, ok := projectStatsMap[*entry.ProjectID]; ok {
				projectStats = ps
			} else {
				projectStats = &ProjectStats{Project: projects[*entry.ProjectID], TotalTime: time.Duration(0)}
				projectStatsMap[*entry.ProjectID] = projectStats
			}
		} else {
			if ps, ok := projectStatsMap[""]; ok {
				projectStats = ps
			} else {
				projectStats = &ProjectStats{Project: nil, TotalTime: time.Duration(0)}
				projectStatsMap[""] = projectStats
			}
		}
		projectStats.TotalTime += entry.TimeInterval.Duration
	}

	// Sort
	userStatsSlice := make([]*UserStats, 0, len(userStatsMap))
	for _, stats := range userStatsMap {
		userStatsSlice = append(userStatsSlice, stats)
	}
	sort.Slice(userStatsSlice, func(i, j int) bool { return userStatsSlice[i].TotalTime > userStatsSlice[j].TotalTime })

	projectStatsSlice := make([]*ProjectStats, 0, len(projectStatsMap))
	for _, stats := range projectStatsMap {
		projectStatsSlice = append(projectStatsSlice, stats)
	}
	sort.Slice(projectStatsSlice, func(i, j int) bool { return projectStatsSlice[i].TotalTime > projectStatsSlice[j].TotalTime })

	return &ClockifyStats{
		TopUsers:    userStatsSlice,
		TopProjects: projectStatsSlice,
	}, nil
}

func (b *Bot) FormatDuration(dur time.Duration) string {
	hours := int(dur.Hours())
	minutes := int(dur.Minutes()) % 60
	return fmt.Sprintf("%v:%02v", hours, minutes)
}

func (b *Bot) FormatClockifyStats(stats *ClockifyStats) string {
	message := ""

	message += fmt.Sprintf("<b>%v</b>\n", b.Config.ReportTitle)

	message += "\n<b>Top Projects:</b>\n"
	for i, projectStats := range stats.TopProjects {
		var projectName string
		if projectStats.Project != nil {
			projectName = projectStats.Project.Name
		} else {
			projectName = "(no project)"
		}
		message += fmt.Sprintf("%v. <code>%v</code> %v\n", i+1, b.FormatDuration(projectStats.TotalTime), projectName)
	}

	message += "\n<b>Top Users:</b>\n"
	for i, userStats := range stats.TopUsers {
		message += fmt.Sprintf("%v. <code>%v</code> %v\n", i+1, b.FormatDuration(userStats.TotalTime), userStats.User.Name)
	}

	return message
}

func (b *Bot) SendMessage(text string) error {
	client := &http.Client{}
	if b.Config.TelegramProxy != "" {
		socksProxy, err := proxy.SOCKS5("tcp", b.Config.TelegramProxy, nil, proxy.Direct)
		if err != nil {
			return fmt.Errorf("proxy.SOCKS5: %w", err)
		}

		client.Transport = &http.Transport{Dial: socksProxy.Dial}
	}

	bot, err := tgbotapi.NewBotAPIWithClient(b.Config.BotToken, client)
	if err != nil {
		return fmt.Errorf("tgbotapi.NewBotAPI: %w", err)
	}

	msg := tgbotapi.NewMessage(b.Config.ChatID, text)
	msg.ParseMode = tgbotapi.ModeHTML

	if _, err := bot.Send(msg); err != nil {
		return fmt.Errorf("Send: %w", err)
	}

	return nil
}

func (b *Bot) RunScheduler() error {
	loc, err := time.LoadLocation(b.Config.Timezone)
	if err != nil {
		return fmt.Errorf("time.LoadLocation: %w", err)
	}
	log.Printf("Running in timezone %v", loc)

	sched := cron.New(cron.WithLocation(loc))
	if _, err := sched.AddFunc(b.Config.CronSchedule, b.runOnce); err != nil {
		return fmt.Errorf("cron.Cron.AddFunc: %w", err)
	}

	log.Printf("Starting the scheduler")
	sched.Run()
	return nil
}

func (b *Bot) runOnce() {
	log.Printf("Woke up!")

	stats, err := b.DownloadClockifyStats()
	if err != nil {
		log.Printf("Error while downloading stats: %v", err)
	}

	message := b.FormatClockifyStats(stats)

	if err := b.SendMessage(message); err != nil {
		log.Fatalf("Error while sending the message: %v", err)
	}

	log.Printf("Successfully sent the message")
}
