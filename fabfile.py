from fabric import task


@task
def upgrade(c):
    c.local("docker build . -t registry.gitlab.com/oxymeal/clockify-report-bot")
    c.local("docker push registry.gitlab.com/oxymeal/clockify-report-bot")

    with c.cd("clockifybot"):
        c.run("docker-compose pull")
        c.run("docker-compose up -d")
