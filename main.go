package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/oxymeal/clockify-report-bot/bot"
)

func parseConfig(path string) (*bot.Config, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, fmt.Errorf("Open: %w", err)
	}
	defer file.Close()

	contents, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, fmt.Errorf("ReadAll: %w", err)
	}

	config := &bot.Config{}
	if err := json.Unmarshal(contents, config); err != nil {
		return nil, fmt.Errorf("json.Unmarshal: %w", err)
	}

	return config, nil
}

func main() {
	configPath := flag.String("c", "config.json", "Path to the config file")
	flag.Parse()

	config, err := parseConfig(*configPath)
	if err != nil {
		log.Fatal(err)
	}

	bot := &bot.Bot{
		Config: config,
	}
	if err := bot.RunScheduler(); err != nil {
		log.Fatal(err)
	}
}
