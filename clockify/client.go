package clockify

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type Client struct {
	APIKey       string
	baseEndpoint string
}

func New(apiKey string) *Client {
	return &Client{
		APIKey:       apiKey,
		baseEndpoint: "https://api.clockify.me/api/v1",
	}
}

func (c *Client) newAPIRequest(method, endpoint string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequest(method, c.baseEndpoint+endpoint, body)
	if err != nil {
		return nil, fmt.Errorf("http.NewRequest: %w", err)
	}

	req.Header.Add("X-Api-Key", c.APIKey)

	return req, nil
}

func (c *Client) doAPIRequest(method, endpoint string, body io.Reader, unmarshalTo interface{}) error {
	req, err := c.newAPIRequest(method, endpoint, body)
	if err != nil {
		return fmt.Errorf("new API request: %w", err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("do request: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		respBody, err := ioutil.ReadAll(resp.Body)

		var respBodyDisplay string
		if err != nil {
			respBodyDisplay = fmt.Sprintf("<unavailable: %v>", err)
		} else {
			respBodyDisplay = string(respBody)
		}

		return fmt.Errorf("server returned status %v, body: %v", resp.Status, respBodyDisplay)
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("read response body: %w", err)
	}

	if err := json.Unmarshal(respBody, unmarshalTo); err != nil {
		return fmt.Errorf("unmarshal response body: %w", err)
	}

	return nil
}

func (c *Client) GetWorkspaces() ([]*Workspace, error) {
	workspaces := []*Workspace{}
	err := c.doAPIRequest(http.MethodGet, "/workspaces", nil, &workspaces)
	return workspaces, err
}

func (c *Client) GetWorkspaceUsers(wid string) ([]*User, error) {
	users := []*User{}
	err := c.doAPIRequest(http.MethodGet, fmt.Sprintf("/workspaces/%v/users", wid), nil, &users)
	return users, err
}

func (c *Client) GetWorkspaceProjects(wid string) ([]*ProjectImpl, error) {
	projects := []*ProjectImpl{}
	err := c.doAPIRequest(http.MethodGet, fmt.Sprintf("/workspaces/%v/projects", wid), nil, &projects)
	return projects, err
}

func (c *Client) GetWorkspaceUserTimeEntries(wid, uid string, start time.Time) ([]*TimeEntryImpl, error) {
	query := url.Values{}
	query.Add("start", start.UTC().Format(time.RFC3339))
	query.Add("page-size", "1000")

	url := fmt.Sprintf("/workspaces/%v/user/%v/time-entries?%v", wid, uid, query.Encode())
	entries := []*TimeEntryImpl{}
	if err := c.doAPIRequest(http.MethodGet, url, nil, &entries); err != nil {
		return nil, err
	}

	// Calculate duration locally, because it's easier than parsing the ISO 8601 duration format
	for _, entry := range entries {
		entry.TimeInterval.Duration = entry.TimeInterval.End.Sub(entry.TimeInterval.Start)
	}

	return entries, nil
}
