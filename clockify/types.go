package clockify

import (
	"time"
)

type Workspace struct {
	ID   string
	Name string
}

type User struct {
	ID   string
	Name string
}

type ProjectImpl struct {
	ID   string
	Name string
}

type TimeEntryImpl struct {
	ID           string
	ProjectID    *string
	UserID       string
	WorkspaceID  string
	TimeInterval TimeInterval
}

type TimeInterval struct {
	Duration time.Duration `json:"-"`
	Start    time.Time
	End      time.Time
}
